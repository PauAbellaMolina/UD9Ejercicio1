import dto.Electrodomestico;
import dto.Lavadora;
import dto.Television;

public class ElectrodomesticoApp {

	public static void main(String[] args) {
		//Creamos el array de objetos Electrodomestico
		Electrodomestico arrayElectrodomesticos[] = new Electrodomestico[10];
		
		//Rellenamos el array con objetos lavadora, television y electrodomestico pasando algunos atributos para usar los tres constructores
		arrayElectrodomesticos[0] = new Lavadora();
		arrayElectrodomesticos[1] = new Lavadora(40, 80);
		arrayElectrodomesticos[2] = new Lavadora(55, "azul", 'C', 85, 25);
		arrayElectrodomesticos[3] = new Lavadora(125, 15);
		arrayElectrodomesticos[4] = new Television();
		arrayElectrodomesticos[5] = new Television(150, "gris", 'A', 75, 55, false);
		arrayElectrodomesticos[6] = new Television(95, "blanco", 'D', 55, 32, true);
		arrayElectrodomesticos[7] = new Television(120, 60);
		arrayElectrodomesticos[8] = new Electrodomestico();
		arrayElectrodomesticos[9] = new Electrodomestico(245, "rojo", 'F', 100);
		
		double totalElectrodomesticos = 0;
		double totalLavadoras = 0;
		double totalTelevisiones = 0;
		
		//Bucle que suma al total del precio final el precio de cada electrodomestico usando el instanceof para llamar al metodo de cada objeto
		for (int cont = 0; cont < arrayElectrodomesticos.length; cont++) {
			if (arrayElectrodomesticos[cont] instanceof Electrodomestico) {
				totalElectrodomesticos += arrayElectrodomesticos[cont].precioFinal();
			}
			if (arrayElectrodomesticos[cont] instanceof Lavadora) {
				totalLavadoras += arrayElectrodomesticos[cont].precioFinal();
			}
			if (arrayElectrodomesticos[cont] instanceof Television) {
				totalTelevisiones += arrayElectrodomesticos[cont].precioFinal();
			}
		}
		
		//Mostramos por consola el total de lavadoras, televisiones y el total de electrodomesticos
		System.out.println("Total Lavadoras: " + totalLavadoras);
		System.out.println("Total Televisiones: " + totalTelevisiones);
		System.out.println("Total todos los electrodomesticos " + totalElectrodomesticos);
	}
}
