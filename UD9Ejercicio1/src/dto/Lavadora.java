package dto;

public class Lavadora extends Electrodomestico {

	//Atributos
	protected int carga;
	
	//Constantes
	final int CARGA = 5;

	//Metodo toString
	public String toString() {
		return "Lavadora [carga=" + carga + "]" + 
		" Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
		+ consumoEnergetico + ", peso=" + peso + "]";
	}
	
	//Constructor por defecto
	public Lavadora() {
		super();
		this.carga=CARGA;
	}

	//Constructor con el precio y peso. El resto por defecto
	public Lavadora(double precioBase, double peso) {
		super();
		this.precioBase=precioBase;
		this.peso=peso;
		this.carga=CARGA;
	}
	
	//Constructor con todos los atributos
	public Lavadora(double precioBase, String color, char consumoEnergetico, double peso, int carga) {
		super(precioBase, color, consumoEnergetico, peso);
		this.carga=carga;
	}

	//Metodo para calcular el precio final
	public double precioFinal() {
		double precioFinalAux = super.precioFinal();
		
		if (carga > 30) {
			precioFinalAux += 50;
		}
		
		return precioFinalAux;
	}
	
	//Get de carga
	public int getCarga() {
		return carga;
	}
}
