package dto;

public class Electrodomestico {

	//Atributos
	protected double precioBase;
	protected String color;
	protected char consumoEnergetico;
	protected double peso;
	
	//Constantes
	final double PRECIOBASE = 100;
	final String COLOR = "blanco";
	final char CONSUMOENERGETICO = 'F';
	final double PESO = 5;

	//Metodo toString
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";
	}

	//Constructor por defecto
	public Electrodomestico() {
		this.precioBase=PRECIOBASE;
		this.color=COLOR;
		this.consumoEnergetico=CONSUMOENERGETICO;
		this.peso=PESO;
	}

	//Constructor con el precio y peso. El resto por defecto
	public Electrodomestico(double precioBase, double peso) {
		this.precioBase=precioBase;
		this.color=COLOR;
		this.consumoEnergetico=CONSUMOENERGETICO;
		this.peso=peso;
	}
	
	//Constructor con todos los atributos
	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		this.precioBase=precioBase;
		this.color=comprobarColor(color);
		this.consumoEnergetico=comprobarConsumoEnergetico(consumoEnergetico);
		this.peso=peso;
	}

	//Metodo para comprovar si el color introducido esta entre los aceptados
	private String comprobarColor(String color) {
		if (color.equalsIgnoreCase("blanco") || color.equalsIgnoreCase("negro") || color.equalsIgnoreCase("rojo") || color.equalsIgnoreCase("azul") || color.equalsIgnoreCase("gris")) {
			return color;
		} else {
			return COLOR;
		}
	}

	//Metodo para comprovar si el caracter de consumo energetico esta entre los aceptados
	private char comprobarConsumoEnergetico(char consumoEnergetico) {
		if (consumoEnergetico == 'A' || consumoEnergetico == 'B' || consumoEnergetico == 'C' || consumoEnergetico == 'D' || consumoEnergetico == 'C' || consumoEnergetico == 'F') {
			return consumoEnergetico;
		} else {
			return CONSUMOENERGETICO;
		}
	}
	
	//Metodo para calcular el precio final
	public double precioFinal() {
		double precioFinalAux = 0;
		
		//Sumamos un bonus al precio dependiendo del consumo electrico
		switch (consumoEnergetico) {
			case 'A':
				precioFinalAux += 100;
				break;
			case 'B':
				precioFinalAux += 80;
				break;
			case 'C':
				precioFinalAux += 60;
				break;
			case 'D':
				precioFinalAux += 50;
				break;
			case 'E':
				precioFinalAux += 30;
				break;
			case 'F':
				precioFinalAux += 10;
				break;
		}
		
		//Sumamos un bonus al precio dependiendo del peso
		if (peso >= 0 && peso < 19) {
			precioFinalAux += 10;
		} else if (peso >= 20 && peso < 49) {
			precioFinalAux += 50;
		} else if (peso >= 50 && peso <= 79) {
			precioFinalAux += 80;
		} else if (peso >= 80) {
			precioFinalAux += 100;
		}
		
		return precioBase+precioFinalAux;
	}
	
	//Getters
	public double getPrecioBase() {
		return precioBase;
	}

	public String getColor() {
		return color;
	}

	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	public double getPeso() {
		return peso;
	}
}
