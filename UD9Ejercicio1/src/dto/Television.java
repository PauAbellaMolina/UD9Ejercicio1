package dto;

public class Television extends Electrodomestico {

	//Atributos
	protected double resolucion;
	protected boolean sintonizadorTdt;
	
	//Constantes
	final double RESOLUCION = 20;
	final boolean SINTONIZADORTDT = false;

	//Metodo toString
	public String toString() {
		return "Television [resolucion=" + resolucion + ", sintonizadorTdt=" + sintonizadorTdt + "]" + 
				" Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";
	}

	//Constructor por defecto
	public Television() {
		super();
		this.resolucion=RESOLUCION;
		this.sintonizadorTdt=SINTONIZADORTDT;
	}
	
	//Constructor con el precio y peso. El resto por defecto
	public Television(double precioBase, double peso) {
		super();
		this.precioBase=precioBase;
		this.peso=peso;
		this.resolucion=RESOLUCION;
		this.sintonizadorTdt=SINTONIZADORTDT;
	}
	
	//Constructor con todos los atributos
	public Television(double precioBase, String color, char consumoEnergetico, double peso, double resolucion, boolean sintonizadorTdt) {
		super(precioBase, color, consumoEnergetico, peso);
		this.resolucion=resolucion;
		this.sintonizadorTdt=sintonizadorTdt;
	}
	
	//Metodo para calcular el precio final
	public double precioFinal() {
		double precioFinalAux = super.precioFinal();
		
		if (resolucion > 40) {
			precioFinalAux += precioBase*0.3;
		}
		if (sintonizadorTdt) {
			precioFinalAux += 50;
		}
		
		return precioFinalAux;
	}
	
	//Getters
	public double getResolucion() {
		return resolucion;
	}
	
	public boolean isSintonizadorTdt() {
		return sintonizadorTdt;
	}

}
